def colors (String color) {
    if ('SUCCESS' == color) {
        return 'good'
    }

    return 'danger'
}

def user() {
    return currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserId()
}

def branch() {
    return scm.branches[0].name
}

def commit() {
    return sh (script: "git log -n 1 --pretty=format:'%H'", returnStdout: true)
}

def call(String canal, String status, String color_status) {
    slackSend channel: canal,
    color: colors(color_status) ,
    message: "*${status}* pipeline ${env.JOB_NAME} branch: ${branch()} commit ${commit()} build: ${env.BUILD_NUMBER} by *${user()}* build ${env.BUILD_NUMBER} by *${user()}* More info at: ${env.BUILD_URL}"
}